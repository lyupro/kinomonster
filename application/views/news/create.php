
<!--Loads 'success' template-->
<?php $this->load->view('templates/success', $result); ?>

<!--Loads 'error' template-->
<?php $this->load->view('templates/error', $result); ?>

<form action="/news/store" method="post">

	<input class="form-control input-lg" type="input" name="news_slug" placeholder="News Slug"></br>
	<input class="form-control input-lg" type="input" name="news_title" placeholder="News Name"></br>
	<textarea class="form-control input-lg" name="news_body" placeholder="New Body"></textarea></br>
	<input class="btn btn-default" type="submit" name="submit" value="Добавить новость">

</form>

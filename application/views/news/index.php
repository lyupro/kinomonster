

<h1><?php echo $title ?></h1>

<!--Loads 'success' template-->
<?php $this->load->view('templates/success', $result); ?>

<!--Loads 'error' template-->
<?php $this->load->view('templates/error', $result); ?>

<p><a href="/news/create/">Добавить новость</a></p>

<?php if(!empty($news)): ?>
	<?php foreach ($news as $key => $value): ?>
		<p>	<a href="/news/<?php echo $value['news_slug']; ?>"><?php echo $value['news_title']; ?></a> |
			<a href="/news/<?php echo $value['news_slug']; ?>/edit">edit</a> |
			<a href="/news/<?php echo $value['news_slug']; ?>/delete">delete</a>
		</p>
	<?php endforeach ?>
<?php endif ?>

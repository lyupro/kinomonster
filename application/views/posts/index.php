

<h1><?php echo $title ?></h1>

<!--Loads 'success' template-->
<?php $this->load->view('templates/success', $result); ?>

<!--Loads 'error' template-->
<?php $this->load->view('templates/error', $result); ?>

<p><a href="/posts/create/">Добавить пост</a></p>

<?php if(!empty($posts)): ?>
	<?php foreach ($posts as $key => $value): ?>
		<p>	<a href="/posts/<?php echo $value['post_slug']; ?>"><?php echo $value['post_title']; ?></a> |
			<a href="/posts/<?php echo $value['post_slug']; ?>/edit">edit</a> |
			<a href="/posts/<?php echo $value['post_slug']; ?>/delete">delete</a>
		</p>
	<?php endforeach ?>
<?php endif ?>

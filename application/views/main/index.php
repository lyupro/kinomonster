<!-- CONTENT (index) start -->

<h2>Новые фильмы</h2>
<hr>
<div class="row">

	<?php foreach ($movies as $key => $value): ?>
		<div class="films_block col-lg-3 col-md-3 col-sm-3 col-xs-6">
			<a href="show.html"><img src="<?php echo $value['movie_poster']?>" alt="<?php echo $value['movie_name']?>"></a>
			<div class="film_label"><a href="show.html"><?php echo $value['movie_name']?></a></div>
		</div>
	<?php endforeach ?>


</div>

<div class="margin-8"></div>

<h2>Новые сериалы</h2>
<hr>
<div class="row">

	<?php foreach ($serials as $key => $value): ?>
		<div class="films_block col-lg-3 col-md-3 col-sm-3 col-xs-6">
			<a href="show.html"><img src="<?php echo $value['movie_poster']?>" alt="<?php echo $value['movie_name']?>"></a>
			<div class="film_label"><a href="show.html"><?php echo $value['movie_name']?></a></div>
		</div>
	<?php endforeach ?>

</div>

<?php foreach ($posts as $key => $value): ?>

	<div class="margin-8"></div>

	<a href="#"><h3><?php echo $value['post_title']?></h3></a>
	<hr>
	<p>
		<?php echo $value['post_body']?>
	</p>
	<a href="/posts/<?php echo $value['post_slug']; ?>" class="btn btn-warning pull-right">Читать...</a>

<?php endforeach ?>

<!--<div class="margin-8"></div>-->
<!---->
<!--<a href="#"><h3>Актер Том Хенкс поделился впечатлением о фестивале</h3></a>-->
<!--<hr>-->
<!--<p>-->
<!--	16 февраля в Лондоне состоялась 67-я церемония вручения наград Британской киноакадемии. Леонардо ДиКаприо, Брэд Питт, Анджелина Джоли, Кейт Бланшетт, Хелен Миррен, Эми Адамс, Кристиан Бэйл, Альфонсо Куарон и другие гости и победители премии — в нашем репортаже.-->
<!--</p>-->
<!--<a href="#" class="btn btn-warning pull-right">читать</a>-->

<div class="margin-8 clear"></div>

<!-- CONTENT (index) end -->

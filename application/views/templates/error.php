
<!--Checks, if in response exists 'error' key-->
<?php if(isset($result['error'])): ?>
	<?php $this->output->set_status_header($result['error'][0]['status_code']); ?>

	<?php foreach ($result['error'] as $key => $value): ?>
		<div class="alert alert-danger" role="alert">
			<?php echo $value['message'] ?>
		</div>
	<?php endforeach ?>

<!--Single version of alert-->
<!--	<div class="alert alert-danger" role="alert">-->
<!--		--><?php //echo $result['error']['message'] ?>
<!--	</div>-->
<?php endif ?>

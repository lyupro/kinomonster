
<!--Checks, if in response exists 'success' key-->
<?php if(isset($result['success'])): ?>
	<div class="alert alert-success" role="alert">
		<?php echo $result['success']['message'] ?>
	</div>
<?php endif ?>

<?php


class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->data['title'] = "КиноМонстр - сайт о кино";

		$this->load->model('news_model');
		// Fills 'news' parameters to the query builder in News Model
		$data_news = [
			'limit'		=>  5,
		];
		$this->data['news_main'] = $this->news_model->getNews($data_news);


		$this->load->model('movie_model');
		// Fills 'movies' parameters to the query builder in News Model
		$data_movies = [
			'category_id'	=>  1,
			'rating'		=>  0,
			'ordering_name'	=>  "movie_rating",
			'limit'			=>  10,
		];
		$this->data['movies_rating'] = $this->movie_model->getMoviesByRating($data_movies);
	}
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Main extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->data['title'] = "Главная страница";

		$this->load->model('movie_model');
		// Fills 'movies' parameters to the query builder in Movies Model
		$data_movies = [
			'category_id'	=>  1,
			'limit'			=>  8,
		];
		$this->data['movies'] = $this->movie_model->getMovies($data_movies);

		// Changes 'movies' parameter to the query builder in Movies Model
		$data_movies['category_id'] = 2;
		$this->data['serials'] = $this->movie_model->getMovies($data_movies);


		$this->load->model('post_model');
		// Fills 'posts' parameters to the query builder in Posts Model
		$data_posts = [
			'limit'		=>  3,
		];
		$this->data['posts'] = $this->post_model->getPosts($data_posts);

		$this->load->view('templates/header', $this->data);
		$this->load->view('main/index', $this->data);
		$this->load->view('templates/footer');
	}
}

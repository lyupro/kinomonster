<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class News extends MY_Controller
{
	/**
	 * News constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('news_model');
	}

	/**
	 * Shows list of News
	 *
	 * @return mixed
	 */
	public function index()
	{
		$this->data['title'] = "Все новости";

		$result		= null;

		// Fills 'news' parameters to the query builder in News Model
		$data_news = [
			'limit'		=>  10,
		];

		if($this->session->flashdata('result'))
		{
			$result = $this->session->flashdata('result');
		}

		// Gets news items from 'news' table
		$get_news = $this->news_model->getNews($data_news);
		// Checks, if 'news' not exist
		if(!$get_news)
		{
			$result['error'][] = [
				'message'		=> "Новости не найдены",
				'status_code'	=> 404,
			];
		}else{
			$this->data['news']		= $get_news;
		}

		if(!is_null($result))
		{
			$this->data['result']	= $result;
		}

		$this->load->view('templates/header', $this->data);
		$this->load->view('news/index', $this->data);
		$this->load->view('templates/footer');
	}

	/**
	 * Shows News create page
	 */
	public function create()
	{
		$this->data['title'] = "Добавить новость";

		$this->load->view('templates/header', $this->data);
		$this->load->view('news/create', $this->data);
		$this->load->view('templates/footer');
	}

	/**
	 * Saves created News
	 */
	public function store()
	{
		$news_slug		= htmlspecialchars($this->input->post('news_slug'));

		// Fills 'news' parameters to the query builder in News Model
		$data_news = [
			'news_slug'		=>  $news_slug,
		];

		$result		= null;

		$get_news_item =  $this->news_model->getNews($data_news);

		// Checks, if current record in database not exists, shows 404 error
		if($get_news_item)
		{
			$result['error'][] = [
				'message'		=> "Данная новость ". $news_slug ." уже существует!",
				'status_code'	=> 409,
			];

			$this->data['result'] = $result;

			$this->load->view('templates/header', $this->data);
			$this->load->view('news/create', $this->data);
			$this->load->view('templates/footer');
			return;
		}

		$news_title		= htmlspecialchars($this->input->post('news_title'));
		$news_body 		= htmlspecialchars($this->input->post('news_body'));

		if($news_slug && $news_title && $news_body)
		{
			// Loads required 'date' helper for 'mdate()' function
			$this->load->helper('date');
			// Analogue of date("Y-m-d H:i:s"); - but with wider possibilities
			$date = mdate("%Y-%m-%d %H:%i:%s", now());

			// Fills 'news' parameters to the query builder in News Model
			$data_news = [
				'news_slug'		=>  $news_slug,
				'news_title'	=>  $news_title,
				'news_body'		=>  $news_body,
				'news_status'	=>  "approved", // DEBUG
				'created_at'	=>  $date,
			];

			// Makes new record in 'news' table
			$news_id_created = $this->news_model->setNews($data_news);
			// Checks, if 'news' item wasn't created
			if(!$news_id_created)
			{
				$result['error'][] = [
					'message'		=> "Ошибка создания ". $news_title,
					'status_code'	=> 500,
				];

				// Checks, if something went wrong with inserting into DB and we got \Exception object
			}elseif(is_object($news_id_created))
			{
				if($news_id_created->getMessage())
				{
					$result['error'][] = [
						'message'		=> $news_id_created->getMessage()
							." at ". $news_id_created->getFile()
							." on ". $news_id_created->getLine() ." line.",
						'status_code'	=> 503,
					];
				}
			}else{
				$result['success'] = [
					'message'		=> "Новость ". $news_title ." успешно создана!",
					'news_id'		=> $news_id_created,
				];
			}
		}else{
			$result['error'][] = [
				'message'		=> "Необходимо заполнить все поля!",
				'status_code'	=> 409,
			];
		}

		$this->data['result'] = $result;

		$this->load->view('templates/header', $this->data);
		$this->load->view('news/create', $this->data);
		$this->load->view('templates/footer');
	}

	/**
	 * Shows specific News by parameter
	 *
	 * @param null $news_slug
	 */
	public function show($news_slug = null)
	{
		// Fills 'news' parameters to the query builder in News Model
		$data_news = [
			'news_slug'		=>  $news_slug,
		];

		// Gets specific news by 'slug' from 'news' table
		$get_news_item = $this->news_model->getNews($data_news);

		// Checks, if current record in database not exists, shows 404 error
		if(empty($get_news_item))
		{
			show_404();
		}

		$this->data['news_title'] = $get_news_item['news_title'];
		$this->data['news_body'] =  $get_news_item['news_body'];

		$this->load->view('templates/header', $this->data);
		$this->load->view('news/view', $this->data);
		$this->load->view('templates/footer');
	}

	/**
	 * Shows News edit page by parameter
	 *
	 * @param $news_slug
	 */
	public function edit($news_slug)
	{
		$this->data['title'] = "Редактировать новость";

		// Fills 'news' parameters to the query builder in News Model
		$data_news = [
			'news_slug'		=>  $news_slug,
		];

		$get_news_item =  $this->news_model->getNews($data_news);

		// Checks, if current record in database not exists, shows 404 error
		if(empty($get_news_item))
		{
			show_404();
		}

		$this->data['news_title'] 	= $get_news_item['news_title'];
		$this->data['news_body'] 	= $get_news_item['news_body'];
		$this->data['news_slug'] 	= $get_news_item['news_slug'];

		$this->load->view('templates/header', $this->data);
		$this->load->view('news/edit', $this->data);
		$this->load->view('templates/footer');
	}

	/**
	 * Updates specific News
	 */
	public function update()
	{
		$news_slug		= htmlspecialchars($this->input->post('news_slug'));
		d($news_slug);

		// Fills 'news' parameters to the query builder in News Model
		$data_news = [
			'news_slug'		=>  $news_slug,
		];

		$result		= null;

		$get_news_item = $this->news_model->getNews($data_news);

		// Checks, if current record in database not exists, shows 404 error
		if(empty($get_news_item))
		{
//			show_404();

			$result['error'][] = [
				'message'		=> "Данная новость ". $news_slug ." не существует!",
				'status_code'	=> 500,
			];

			$this->session->set_flashdata('result', $result);

			// Could be some issues with redirection due to it's specific functionality
			// If you don't see expected result, try to test with other News item, that you didn't use before
			redirect('/news', 'index');
		}

		$news_title		= htmlspecialchars($this->input->post('news_title'));
		$news_body		= htmlspecialchars($this->input->post('news_body'));

		if($news_slug && $news_title && $news_body)
		{
			// Loads required 'date' helper for 'mdate()' function
			$this->load->helper('date');
			// Analogue of date("Y-m-d H:i:s"); - but with wider possibilities
			$date = mdate("%Y-%m-%d %H:%i:%s", now());

			// Fills 'news' parameters to the query builder in News Model
			$data_news = [
				'news_slug'		=>  $news_slug,
				'news_title'	=>  $news_title,
				'news_body'		=>  $news_body,
				'updated_at'	=>  $date,
			];

			// Updates News item record in 'news' table
			$news_id_updated = $this->news_model->updateNews($data_news);
			// Checks, if 'news' item wasn't updated
			if(!$news_id_updated) {
				$result['error'][] = [
					'message' => "Ошибка редактирования " . $news_title,
					'status_code' => 500,
				];
			// Checks, if something went wrong with inserting into DB and we got \Exception object
			}elseif(is_object($news_id_updated)){
				if($news_id_updated->getMessage()){
					$result['error'][] = [
						'message'		=> $news_id_updated->getMessage()
							." at ". $news_id_updated->getFile()
							." on ". $news_id_updated->getLine() ." line.",
						'status_code'	=> 503,
					];
				}
			}else{
				$result['success'] = [
					'message'		=> "Новость ". $news_title ." успешно отредактирована!",
					'news_id'		=> $news_id_updated,
				];
			}
		}else{
			$result['error'][] = [
				'message'		=> "Необходимо заполнить все поля!",
				'status_code'	=> 409,
			];
		}

		$this->data['result'] = $result;

		$this->session->set_flashdata('result', $result);

		// Could be some issues with redirection due to it's specific functionality
		// If you don't see expected result, try to test with other News item, that you didn't use before
		redirect('/news', 'index');
	}

	/**
	 * Deletes specific News by parameter
	 *
	 * @param null $news_slug
	 */
	public function delete($news_slug = null)
	{
		$this->data['title'] = "Удалить новость";

		// Fills 'news' parameters to the query builder in News Model
		$data_news = [
			'news_slug'		=>  $news_slug,
		];

		$result		= null;

		$get_news_item = $this->news_model->getNews($data_news);

		// Checks, if current record in database not exists, shows 404 error
		if(empty($get_news_item))
		{
//			show_404();

			$result['error'][] = [
				'message'		=> "Данная новость ". $news_slug ." не существует!",
				'status_code'	=> 500,
			];
		}else{
			$news_deleted = $this->news_model->deleteNews($news_slug);

			// Checks, if something went wrong with inserting into DB and we got \Exception object
			if(is_object($news_deleted)){
				if($news_deleted->getMessage()){
					$error['error'][] = [
						'message'		=> $news_deleted->getMessage()
							." at ". $news_deleted->getFile()
							." on ". $news_deleted->getLine() ." line.",
						'status_code'	=> 503,
					];
				}
			}

			// Checks, if 'news' item wasn't deleted
			if(!$news_deleted)
			{
				$result['error'][] = [
					'message'		=> "Ошибка удаления ". $news_slug,
					'status_code'	=> 500,
				];
			}else{
				$result['success'] = [
					'message'		=> "Новость ". $news_slug ." успешно удалена",
					'status_code'	=> 200,
				];
			}
		}

		$this->session->set_flashdata('result', $result);

		// Could be some issues with redirection due to it's specific functionality
		// If you don't see expected result, try to test with other News item, that you didn't use before
		redirect('/news', 'index');
	}
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Post extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('post_model');
	}

	/**
	 * Shows list of Posts
	 *
	 * @return mixed
	 */
	public function index()
	{
		$this->data['title'] = "Все посты";

		$result		= null;

		// Fills 'posts' parameters to the query builder in Post Model
		$data_posts = [
			'limit'		=>  10,
		];

		if($this->session->flashdata('result'))
		{
			$result = $this->session->flashdata('result');
		}

		// Gets posts items from 'posts' table
		$get_posts = $this->post_model->getPosts($data_posts);
		// Checks, if 'posts' not exist
		if(!$get_posts)
		{
			$result['error'][] = [
				'message'		=> "Посты не найдены",
				'status_code'	=> 404,
			];
		}else{
			$this->data['posts']		= $get_posts;
		}

		if(!is_null($result))
		{
			$this->data['result']	= $result;
		}

		$this->load->view('templates/header', $this->data);
		$this->load->view('posts/index', $this->data);
		$this->load->view('templates/footer');
	}

	/**
	 * Shows specific Posts by parameter
	 *
	 * @param null $post_slug
	 */
	public function show($post_slug = null)
	{
		// Fills 'post' parameters to the query builder in Post Model
		$data_post = [
			'post_slug'		=>  $post_slug,
		];

		// Gets specific post by 'slug' from 'post' table
		$get_post_item = $this->post_model->getPosts($data_post);

		// Checks, if current record in database not exists, shows 404 error
		if(empty($get_post_item))
		{
			show_404();
		}

		$this->data['post_title'] = $get_post_item['post_title'];
		$this->data['post_body'] =  $get_post_item['post_body'];

		$this->load->view('templates/header', $this->data);
		$this->load->view('posts/view', $this->data);
		$this->load->view('templates/footer');
	}
}

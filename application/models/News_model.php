<?php


class News_model extends CI_Model
{
	/**
	 * News_model constructor.
	 */
	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * Get list of News by parameters
	 *
	 * @param array $params
	 * @return array|bool|Exception
	 */
	public function getNews(array $params): array
	{
		if(!empty($params) && count($params) > 0)
		{
			try{
				$news_slug		=	$params['news_slug'];
				// Checks, if 'news_slug' parameter exists, gets specific record by 'news_slug' from 'news' table
				if(isset($news_slug))
				{
					// Gets specific news by 'news_slug' from 'news' table
					$query = $this->db->get_where('news', ['news_slug' => $news_slug]);
					return $query->row_array();
				// Gets all records by parameters from 'news' table
				}else{
					$news_status		=	$params['news_status'];
					// Checks, if 'news_status' field wasn't filled
					if(!isset($news_status))
					{
						$news_status = "approved";
					}

					$ordering		=	$params['ordering'];
					// Checks, if 'ordering' field wasn't filled
					if(!isset($ordering))
					{
						$ordering = "DESC";
					}

					$ordering_name		=	$params['ordering_name'];
					// Checks, if 'ordering_name' field wasn't filled
					if(!isset($ordering_name))
					{
						$ordering_name = "created_at";
					}

					$start     = $params['start'];
					// Checks, if 'start' field wasn't filled
					if(!isset($start))
					{
						$start = 0;
					}

					$limit     = $params['limit'];
					// Checks, if 'limit' field wasn't filled
					if(!isset($limit))
					{
						$limit = 10;
					}

					$query = $this->db
						->where('news_status', $news_status)
						->order_by($ordering_name, $ordering)
						->limit($limit, $start)
						->get('news');

					return $query->result_array();
				}
			}catch (\Exception $exception){
				return $exception;
			}
		}

		return false;
	}

	/**
	 * Get specific News item by 'id' parameter
	 *
	 * @param int $param
	 * @return bool|Exception
	 */
	public function getNewsByParam(int $param)
	{
		if(!empty($param))
		{
			try{
				// Gets specific news by 'id' from 'news' table
				$query = $this->db->get_where('news', ['id' => $param]);
				return $query->row_array();
			}catch (\Exception $exception){
				return $exception;
			}
		}

		return false;
	}

	/**
	 * Save specific News item by parameters
	 *
	 * @param array $params
	 * @return int | Exception
	 */
	public function setNews(array $params): int
	{
		if(!empty($params) && count($params) > 0)
		{
			try{
				$news_slug		=	$params['news_slug'];
				$news_title		=	$params['news_title'];
				$news_body		=	$params['news_body'];
				$news_status	=	$params['news_status'];
				$created_at		=	$params['created_at'];

				$data = [
					'news_slug'		=>	$news_slug,
					'news_title'	=>	$news_title,
					'news_body'		=>	$news_body,
					'news_status'	=>	$news_status,
					'created_at'	=>	$created_at,
				];

				$this->db->insert('news', $data);

				return $this->db->insert_id();
			}catch (\Exception $exception){
				return $exception;
			}
		}

		return false;
	}

	/**
	 * Update specific News item by parameters
	 *
	 * @param array $params
	 * @return int | Exception
	 */
	public function updateNews(array $params): int
	{
		if(!empty($params) && count($params) > 0)
		{
			try{
				$news_slug		=	$params['news_slug'];
				// Checks, if 'news_slug' field was filled
				if(isset($news_slug))
				{
					$data['news_slug'] = $news_slug;
				}else{
					return false;
				}

				$news_title		=	$params['news_title'];
				// Checks, if 'news_title' field was filled
				if(isset($news_title))
				{
					$data['news_title'] = $news_title;
				}

				$news_body		=	$params['news_body'];
				// Checks, if 'news_body' field was filled
				if(isset($news_body))
				{
					$data['news_body'] = $news_body;
				}

				$news_status	=	$params['news_status'];
				// Checks, if 'news_status' field was filled
				if(isset($news_status))
				{
					$data['news_status'] = $news_status;
				}

				$updated_at		=	$params['updated_at'];
				// Checks, if 'updated_at' field was filled
				if(isset($updated_at))
				{
					$data['updated_at'] = $updated_at;
				}

				return $this->db->update('news', $data, ['news_slug' => $news_slug]);

				return $this->db->insert_id();
			}catch (\Exception $exception){
				return $exception;
			}
		}

		return false;
	}

	/**
	 * Delete specific News item by 'news_slug' parameters
	 *
	 * @param $param
	 * @return bool|Exception
	 */
	public function deleteNews($param): bool
	{
		if(!empty($param))
		{
			try{
				$query = $this->db->delete('news', ['news_slug' => $param]);

				return $query;
			}catch (\Exception $exception){
				return $exception;
			}
		}

		return false;
	}
}

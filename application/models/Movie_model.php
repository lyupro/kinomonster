<?php


class Movie_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * Get list of Movies by parameters
	 *
	 * @param array $params
	 * @return array|bool|Exception
	 */
	public function getMovies(array $params): array
	{
		if(!empty($params) && count($params) > 0)
		{
			try{
				$movie_slug		=	$params['movie_slug'];
				// Checks, if 'news_slug' parameter exists, gets specific record by 'news_slug' from 'news' table
				if(isset($movie_slug))
				{
					// Gets specific news by 'news_slug' from 'news' table
					$query = $this->db->get_where('movies', ['movie_slug' => $movie_slug]);
					return $query->row_array();
				// Gets all records by parameters from 'news' table
				}else{
					$category_id		=	$params['category_id'];
					// Checks, if 'category_id' field wasn't filled
					if(!isset($category_id))
					{
						$category_id = 1;
					}

					$ordering		=	$params['ordering'];
					// Checks, if 'ordering' field wasn't filled
					if(!isset($ordering))
					{
						$ordering = "DESC";
					}

					$ordering_name		=	$params['ordering_name'];
					// Checks, if 'ordering_name' field wasn't filled
					if(!isset($ordering_name))
					{
						$ordering_name = "created_at";
					}

					$start     = $params['start'];
					// Checks, if 'start' field wasn't filled
					if(!isset($start))
					{
						$start = 0;
					}

					$limit     = $params['limit'];
					// Checks, if 'limit' field wasn't filled
					if(!isset($limit))
					{
						$limit = 10;
					}

					$query = $this->db
						->where('category_id', $category_id)
						->order_by($ordering_name, $ordering)
						->limit($limit, $start)
						->get('movies');

					return $query->result_array();
				}
			}catch (\Exception $exception){
				return $exception;
			}
		}

		return false;
	}

	/**
	 * Get rating list of Movies by parameters
	 *
	 * @param array $params
	 * @return array|bool|Exception
	 */
	public function getMoviesByRating(array $params): array
	{
		if(!empty($params) && count($params) > 0)
		{
			try{
				$category_id		=	$params['category_id'];
				// Checks, if 'category_id' field wasn't filled
				if(!isset($category_id))
				{
					$category_id = 1;
				}

				$rating     = $params['rating'];
				// Checks, if 'rating' field wasn't filled
				if(!isset($rating))
				{
					$rating = 0;
				}

				$ordering		=	$params['ordering'];
				// Checks, if 'ordering' field wasn't filled
				if(!isset($ordering))
				{
					$ordering = "DESC";
				}

				$ordering_name		=	$params['ordering_name'];
				// Checks, if 'ordering_name' field wasn't filled
				if(!isset($ordering_name))
				{
					$ordering_name = "created_at";
				}

				$start     = $params['start'];
				// Checks, if 'start' field wasn't filled
				if(!isset($start))
				{
					$start = 0;
				}

				$limit     = $params['limit'];
				// Checks, if 'limit' field wasn't filled
				if(!isset($limit))
				{
					$limit = 10;
				}

				$query = $this->db
					->where('category_id', $category_id)
					->where('movie_rating >', $rating)
					->order_by($ordering_name, $ordering)
					->limit($limit, $start)
					->get('movies');

				return $query->result_array();
			}catch (\Exception $exception){
				return $exception;
			}
		}

		return false;
	}
}

<?php


class Post_model extends CI_Model
{
	/**
	 * Post_model constructor.
	 */
	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * Get list of Posts by parameters
	 *
	 * @param array $params
	 * @return array|bool|Exception
	 */
	public function getPosts(array $params): array
	{
		if(!empty($params) && count($params) > 0)
		{
			try{
				$post_slug		=	$params['post_slug'];
				// Checks, if 'post_slug' parameter exists, gets specific record by 'post_slug' from 'posts' table
				if(isset($post_slug))
				{
					// Gets specific post by 'post_slug' from 'posts' table
					$query = $this->db->get_where('posts', ['post_slug' => $post_slug]);
					return $query->row_array();
					// Gets all records by parameters from 'posts' table
				}else{
					$post_status		=	$params['post_status'];
					// Checks, if 'post_status' field wasn't filled
					if(!isset($post_status))
					{
						$post_status = "approved";
					}

					$ordering		=	$params['ordering'];
					// Checks, if 'ordering' field wasn't filled
					if(!isset($ordering))
					{
						$ordering = "DESC";
					}

					$ordering_name		=	$params['ordering_name'];
					// Checks, if 'ordering_name' field wasn't filled
					if(!isset($ordering_name))
					{
						$ordering_name = "created_at";
					}

					$start     = $params['start'];
					// Checks, if 'start' field wasn't filled
					if(!isset($start))
					{
						$start = 0;
					}

					$limit     = $params['limit'];
					// Checks, if 'limit' field wasn't filled
					if(!isset($limit))
					{
						$limit = 10;
					}

					$query = $this->db
						->where('post_status', $post_status)
						->order_by($ordering_name, $ordering)
						->limit($limit, $start)
						->get('posts');

					return $query->result_array();
				}
			}catch (\Exception $exception){
				return $exception;
			}
		}

		return false;
	}
}
